package thief.main.menus.handler;

import thief.main.menus.BlackStoreInventory;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;

import thief.main.menus.BuyItemsMenu;
import thief.main.menus.SellItemsMenu;

public class BlackStoreInventoryHandler {

	BlackStoreInventory mainInv = BlackStoreInventory.getInstance();
	BuyItemsMenu buyItems = BuyItemsMenu.getInstance();
	SellItemsMenu sellItems = SellItemsMenu.getInstance();
	private static BlackStoreInventoryHandler instance = null;

	public static BlackStoreInventoryHandler getInstance() {
		if (instance == null) {
			instance = new BlackStoreInventoryHandler();
		}

		return instance;

	}

	public void onClick(InventoryClickEvent e, JavaPlugin plugin) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().equals(mainInv.inventories.get(p))) {
			if (e.getSlot() == 26) {
				e.getWhoClicked().closeInventory();
			}
			if (e.getSlot() != 23 && e.getSlot() != 21) {
				e.setCancelled(true);
			}
			
			if(e.getSlot() == 23) {
				if(buyItems.inventories.isEmpty() || buyItems.inventories.get(p) == null) {
					buyItems.createMenu(p, plugin);
				}
				p.openInventory(buyItems.inventories.get(p));

			}
			
			if(e.getSlot() == 21) {
				if(SellItemsMenu.inventories.isEmpty() || SellItemsMenu.inventories.get(p) == null) {
					sellItems.createMenu(p, plugin);
				}
				p.openInventory(SellItemsMenu.inventories.get(p));
			}
		}
	}
}
