package thief.main.menus.handler;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import thief.main.database.DataBaseBuy;
import thief.main.database.DataBaseLimit;
import thief.main.Thief;
import thief.main.menus.BlackStoreInventory;
import thief.main.menus.BuyItemsMenu;
import net.milkbowl.vault.economy.Economy;
import thief.main.obj.Items;

import java.util.List;


public class BuyItemsMenuHandler {

	private final Economy econ = Thief.getEconomy();

	BlackStoreInventory mainInv = BlackStoreInventory.getInstance();
	DataBaseBuy dbBuy = DataBaseBuy.getInstance();
	DataBaseLimit dbLimit = DataBaseLimit.getInstance();
	BuyItemsMenu buyItems = BuyItemsMenu.getInstance();

	private static BuyItemsMenuHandler instance = null;

	public static BuyItemsMenuHandler getInstance() {
		if (instance == null) {
			instance = new BuyItemsMenuHandler();
		}

		return instance;

	}

	public void onClick(InventoryClickEvent e, JavaPlugin plugin) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().equals(buyItems.inventories.get(p))) {
			if (e.getSlot() == 18) {
				if (mainInv.inventories.isEmpty() || mainInv.inventories.get(p) == null) {
					mainInv.createMenu(plugin, p);
				}

				p.openInventory(mainInv.inventories.get(p));
			}

			if (e.getSlot() == 11 || e.getSlot() == 15 || e.getSlot() == 22 || e.getSlot() == 29 || e.getSlot() == 33) {
					if (dbLimit.getLimit(p.getName()) >= e.getCurrentItem().getAmount()) {
						int amount = e.getCurrentItem().getAmount();
						List<Items> savedBuyItems = dbBuy.getSavedBuyItems();
						for (Items savedBuyItem : savedBuyItems) {
							ItemStack item = new ItemStack(savedBuyItem.getItem().getType());
							int price = savedBuyItem.getPrice();
							if (e.getCurrentItem().getType().equals(item.getType())) {

								if (price * amount <= econ.getBalance(p)) {

									if (p.getInventory().firstEmpty() == -1) {
										String fullInventory = plugin.getConfig().getString("fullInventory");
										fullInventory = fullInventory.replace("&", "\u00a7");
										p.sendMessage(fullInventory);
									} else {
										item.setAmount(amount);
										p.getInventory().addItem(item);
										String priceFull = String.valueOf(price * amount);
										econ.withdrawPlayer(p, price * amount);
										String successBuy = plugin.getConfig().getString("successBuy");
										successBuy = successBuy.replace("&", "\u00a7").replace("%price%", priceFull)
												.replace("%limit%", String.valueOf(dbLimit.getLimit(p.getName())));
										p.sendMessage(successBuy);
										dbLimit.setLimit(p.getName(), dbLimit.getLimit(p.getName()) - e.getCurrentItem().getAmount());

									}
									e.setCancelled(true);
									return;
								} else {
									String noMoney = plugin.getConfig().getString("noMoney");
									noMoney = noMoney.replace("&", "\u00a7");
									p.sendMessage(noMoney);

									e.setCancelled(true);
									return;
								}
							}
						}
					} else {
						String messageLimit = plugin.getConfig().getString("limitExceeded");
						messageLimit = messageLimit.replace("&", "\u00a7").replace("%limit%", String.valueOf(dbLimit.getLimit(p.getName())));
						p.sendMessage(messageLimit);
					}
				}
			e.setCancelled(true);
			}
			if (e.getSlot() == 17) {
				increaseItem.increase(dbBuy.getSavedBuyItems(), e, plugin, "buyItemsLore");
			}
			if (e.getSlot() == 35) {
				decreaseItem.decrease(dbBuy.getSavedBuyItems(), e, plugin, "buyItemsLore");
			}

		}
	}

