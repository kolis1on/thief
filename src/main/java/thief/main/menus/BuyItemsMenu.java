package thief.main.menus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import thief.main.database.DataBaseBuy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import thief.main.obj.Items;

public class BuyItemsMenu {

	public HashMap<Player,Inventory> inventories = new HashMap<Player,Inventory>();
    HeadDatabaseAPI api = new HeadDatabaseAPI();
	public Inventory inv = null;
	DataBaseBuy dbBuy = DataBaseBuy.getInstance();

	private static BuyItemsMenu instance = null;

	public static BuyItemsMenu getInstance() {
		if (instance == null) {
			instance = new BuyItemsMenu();
		}

		return instance;

	}


	public void openMenu(Player player, JavaPlugin plugin) {


		player.openInventory(createMenu(player, plugin));

	}


	public Inventory createMenu(Player player, JavaPlugin plugin) {

		List<Items> itemsMap = dbBuy.getSavedBuyItems();

		int[] slotsNumber = { 11, 15, 22, 29, 33 };
		String name = plugin.getConfig().getString("menus.buy");
		name = name.replace("&", "\u00a7");
		inv = Bukkit.createInventory(player, 45, name);
		for (Items items : itemsMap) {
			ItemStack item = items.getItem();
			int price = items.getPrice();
			int slot = items.getSlot();

		item.setAmount(16);
		ItemMeta meta = item.getItemMeta();

		ArrayList<String> lore = (ArrayList<String>) plugin.getConfig().getList("buyItemsLore");
		String priceFull = String.valueOf(price * 16);

		lore = (ArrayList<String>) lore.stream().map(p -> p.replace("&", "\u00a7").replace("%price%", priceFull)).collect(Collectors.toList());
		meta.setLore(lore);

		item.setItemMeta(meta);

		inv.setItem(slot, item);

	}

	ConfigurationSection section =  plugin.getConfig().getConfigurationSection("bothMenuFill");
        for(String s: section.getKeys(false)) {

		ConfigurationSection items =  plugin.getConfig().getConfigurationSection("bothMenuFill.items");
		for(String i: items.getKeys(false)) {

			ItemStack item = new ItemStack(Material.valueOf(plugin.getConfig().getString("bothMenuFill.items."+i+".material")));
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(plugin.getConfig().getString("bothMenuFill.items."+i+".displayName").replace("&", "\u00a7"));

			if(plugin.getConfig().getString("bothMenuFill.items."+i+".lore") != null && plugin.getConfig().getString("bothMenuFill.items."+i+".lore").equals("false")) {

			}else {
				List<String> lore = (List<String>) plugin.getConfig().getList("bothMenuFill.items."+i+".lore");

				lore = lore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
				itemMeta.setLore(lore);
			}
			item.setItemMeta(itemMeta);

			ArrayList<Integer> slots = (ArrayList<Integer>) plugin.getConfig().getIntegerList("bothMenuFill.items."+i+".slots");

			for(int slot: slots) {
				inv.setItem(slot, item);
			}
		}


	}

	ItemStack back = api.getItemHead("7827");
	ItemMeta backItemMeta = back.getItemMeta();

	String backName = "&7Назад";
	backName = backName.replace("&", "\u00a7");
        backItemMeta.setDisplayName(backName);
        back.setItemMeta(backItemMeta);

        inv.setItem(18, back);
        inv.setItem(26, info(plugin));
        inv.setItem(17, increaseAmount(plugin));
        inv.setItem(35, decreaseAmount(plugin));

        inventories.put(player, inv);
        return inv;

}

	public ItemStack increaseAmount(JavaPlugin plugin) {
		ItemStack item = api.getItemHead("7825");
		ItemMeta itemMeta = item.getItemMeta();

		String displayName = plugin.getConfig().getString("increaseItem.displayName");
		displayName = displayName.replace("&","\u00a7");
		itemMeta.setDisplayName(displayName);
		if(plugin.getConfig().getString("increaseItem.lore") != null && plugin.getConfig().getString("increaseItem.lore").equals("false")) {

		}else {
			List<String> lore = (List<String>) plugin.getConfig().getList("increaseItem.lore");

			lore = lore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
			itemMeta.setLore(lore);
		}

		item.setItemMeta(itemMeta);
		return item;

	}

	public ItemStack decreaseAmount(JavaPlugin plugin) {
		ItemStack item = api.getItemHead("7824");
		ItemMeta itemMeta = item.getItemMeta();


		String displayName = plugin.getConfig().getString("decreaseItem.displayName");
		displayName = displayName.replace("&","\u00a7");
		itemMeta.setDisplayName(displayName);
		if(plugin.getConfig().getString("decreaseItem.lore") != null && plugin.getConfig().getString("decreaseItem.lore").equals("false")) {

		}else {
			List<String> lore = (List<String>) plugin.getConfig().getList("decreaseItem.lore");

			lore = lore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
			itemMeta.setLore(lore);
		}

		item.setItemMeta(itemMeta);
		return item;

	}

	public ItemStack info(JavaPlugin plugin) {
		ItemStack item = new ItemStack(Material.WRITABLE_BOOK);
		ItemMeta itemMeta = item.getItemMeta();


		String displayName = plugin.getConfig().getString("infoItem.displayName");
		displayName = displayName.replace("&","\u00a7");
		itemMeta.setDisplayName(displayName);
		if(plugin.getConfig().getString("infoItem.lore") != null && plugin.getConfig().getString("infoItem.lore").equals("false")) {

		}else {
			List<String> lore = (List<String>) plugin.getConfig().getList("infoItem.lore");

			lore = lore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
			itemMeta.setLore(lore);
		}

		item.setItemMeta(itemMeta);
		return item;

	}
}
