package thief.main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import thief.main.database.DataBaseBuy;
import thief.main.database.DataBaseLimit;
import thief.main.database.DataBaseSell;
import thief.main.menus.BlackStoreInventory;
import me.clip.placeholderapi.PlaceholderAPI;
import thief.main.obj.Config;

public class Timer {
	static BlackStoreInventory mainInv = BlackStoreInventory.getInstance();
	static DataBaseSell dbSell = DataBaseSell.getInstance();
	static DataBaseBuy dbBuy = DataBaseBuy.getInstance();
	static DataBaseLimit dbLimit = DataBaseLimit.getInstance();
	static boolean endTime = false;

	public static int time;

	public static void startTimer(JavaPlugin plugin) {
		time = plugin.getConfig().getInt("time");
		Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () ->{
				if (time != 1) {
					time--;
				} else {
					time = 10800;
					dbSell.createSellItems();
					dbBuy.createBuyItems();
					endTime = true;
					plugin.saveConfig();
					dbLimit.getAllUsers().forEach((name) -> {
						dbLimit.setLimit(name, 256);
					});

				}
				if (!Bukkit.getOnlinePlayers().isEmpty()) {

					for (Player player : Bukkit.getOnlinePlayers()) {

						if (endTime) {
							List<String> lore = (List<String>) plugin.getConfig().getList("timeEndMessage");
							List<String> loreReplace = new ArrayList<String>();

							for (String q : lore) {
								String str = PlaceholderAPI.setPlaceholders(player, q);
								str = str.replace("&", "\u00a7");
								loreReplace.add(str);
							}
							loreReplace.forEach((string) -> {
								Bukkit.broadcastMessage(string);
							});
							player.playSound(player.getLocation(), Sound.BLOCK_BUBBLE_COLUMN_BUBBLE_POP, 1, 2);

							endTime = false;

						}
						if (Handler.open.get(player) != null && Handler.open.get(player)) {

							int hours = time / 3600;
							int minutes = (time % 3600) / 60;
							int seconds = time % 60;

							String timeFormat = String.format("%02d:%02d:%02d", hours, minutes, seconds);

							List<String> lore = (List<String>) plugin.getConfig()
									.getList("mainMenuFill.items.book.lore");

							lore = lore.stream()
									.map(q -> q.replace("&", "\u00a7").replace("%time%", timeFormat).replace(
											"%limit%", String.valueOf(dbLimit.getLimit(player.getName()))))
									.collect(Collectors.toList());

							ItemStack item = new ItemStack(Material
									.valueOf(plugin.getConfig().getString("mainMenuFill.items.book.material")));
							ItemMeta itemMeta = item.getItemMeta();

							String displayName = plugin.getConfig()
									.getString("mainMenuFill.items.book.displayName");
							displayName = displayName.replace("&", "\u00a7");

							itemMeta.setDisplayName(displayName);
							itemMeta.setLore(lore);

							item.setItemMeta(itemMeta);

							mainInv.inventories.get(player).setItem(18, item);
						}

					}
				} else {
					endTime = false;
				}

		},0L, 20L);


	}

}
