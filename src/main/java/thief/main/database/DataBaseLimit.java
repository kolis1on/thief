package thief.main.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DataBaseLimit {
	public Connection connection;
	private static DataBaseLimit instance = null;
	DataBase db = DataBase.getInstance();

	public static DataBaseLimit getInstance() {
		if (instance == null) {
			instance = new DataBaseLimit();
		}

		return instance;

	}




	public int getLimit(String name) {
		int result = 0;
		try {

			Statement statement = db.getConnection().createStatement();
			ResultSet res = statement.executeQuery("SELECT `limit` FROM `thief_limit` WHERE `player` = '" + name + "'");
			res.next();
			result = res.getInt("limit");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	public void setLimit(String name, int limit) {
		try {

			Statement statement = db.getConnection().createStatement();
			statement.executeUpdate(
					"UPDATE `thief_limit` SET `limit` = '" + limit + "' WHERE `player` = '" + name + "'");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void onJoin(String name, int limit) {
		if (!playerExist(name)) {
			try {

				Statement statement = db.getConnection().createStatement();
				statement.executeUpdate(
						"INSERT INTO `thief_limit` (`player`, `limit`) VALUES ('" + name + "', " + limit + ")");

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private boolean playerExist(String name) {
		boolean check = false;

		try {

			Statement statement = db.getConnection().createStatement();

			ResultSet res = statement
					.executeQuery("SELECT `player` FROM `thief_limit` WHERE `player` = '" + name + "'");

			if (res.next()) {
				check = true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;

	}

	public List<String> getAllUsers() {
		List<String> userList = new ArrayList<>();
		try {
			Statement statement = db.getConnection().createStatement();

			ResultSet res = statement.executeQuery("SELECT * FROM `thief_limit`");
			
			while(res.next()) {
				userList.add(res.getString("player"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userList;
	}
}
