package thief.main.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaveConfig {
    public Config config;
}
